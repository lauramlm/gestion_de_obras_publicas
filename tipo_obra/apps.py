from django.apps import AppConfig


class TipoObraConfig(AppConfig):
    name = 'tipo_obra'
