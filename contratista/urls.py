from django.urls import path
from .views import  ContratistaList, ContratistaCreate, ContratistaUpdate, ContratistaDelete 

app_name = 'contratista'

urlpatterns = [
    path('list/', ContratistaList.as_view(), name='list'),
    path('create/', ContratistaCreate.as_view(), name='create'),
    path('update/<int:pk>/', ContratistaUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', ContratistaDelete.as_view(), name='delete')

    
]
