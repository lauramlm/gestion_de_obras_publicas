from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Contratista
from .forms import ContratistaForm

class ContratistaList (LoginRequiredMixin, ListView):
    model = Contratista
    template_name = "contratista/index.html"

class ContratistaCreate(LoginRequiredMixin, CreateView):
    model = Contratista
    form_class = ContratistaForm
    success_url = reverse_lazy('contratista:create')
    template_name = 'contratista/create.html'

class ContratistaUpdate(LoginRequiredMixin, UpdateView):
    model = Contratista
    form_class = ContratistaForm
    success_url = reverse_lazy('contratista:list')
    template_name = 'contratista/update.html'

class ContratistaDelete(LoginRequiredMixin, DeleteView):
    model = Contratista
    success_url = reverse_lazy('contratista:list')
    template_name = 'contratista/delete.html'
