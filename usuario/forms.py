import re
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

class RegsitroForm(UserCreationForm):
    
    class Meta: 
        model = User 
        fields = [
            'username',
            'first_name',
            'last_name',
            'email'
        ]
    def __init__(self, *args, **kwargs):
        super(RegsitroForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}
            

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})
