from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import views as auth_views
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy

from .forms import RegsitroForm

class LoginView(auth_views.LoginView):
    redirect_authenticated_user = True

class LogoutView(auth_views.LogoutView):
    pass

class UsuarioCreate(LoginRequiredMixin, CreateView):
    model = User
    form_class = RegsitroForm
    success_url = reverse_lazy('obra:list')
    template_name = "registration/create.html"

class UsuarioList(ListView):
    model = User
    template_name = "registration/index.html"

class UsuarioUpdate(LoginRequiredMixin, UpdateView):
    model = User
    form_class = RegsitroForm
    success_url = reverse_lazy('usuario:list')
    template_name = 'registration/update.html'

class UsuarioDelete(LoginRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy('usuario:list')
    template_name = 'registration/delete.html'
 




