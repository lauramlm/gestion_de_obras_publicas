from django.urls import path
from .views import UsuarioCreate, LoginView, UsuarioList, UsuarioDelete, UsuarioUpdate, LogoutView

app_name = "usuario"

urlpatterns = [
    path('list/', UsuarioList.as_view(), name='list'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('create/',UsuarioCreate.as_view(), name='create'),
    path('update/<int:pk>/', UsuarioUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', UsuarioDelete.as_view(), name='delete')
]