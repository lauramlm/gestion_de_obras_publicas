from django.db import models

class Parametrizacion(models.Model):
    nombre = models.CharField(max_length=60)
    
    porcentaje_valor_multa = models.IntegerField()

    porcentaje_desfase = models.IntegerField()

    porcentaje_desfase_incremental = models.IntegerField()

    def __str__(self):
        return self.nombre
