from django.shortcuts import render

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Parametrizacion
from .forms import ParametrizacionForm


class ParametrizacionList (LoginRequiredMixin, ListView):
    model = Parametrizacion
    template_name = "parametrizacion/index.html"

class ParametrizacionCreate(LoginRequiredMixin, CreateView):
    model = Parametrizacion
    form_class = ParametrizacionForm
    success_url = reverse_lazy('parametrizacion:list')
    template_name = 'parametrizacion/create.html'


class ParametrizacionUpdate(LoginRequiredMixin, UpdateView):
    model = Parametrizacion
    form_class = ParametrizacionForm
    success_url = reverse_lazy('parametrizacion:list')
    template_name = 'parametrizacion/update.html'

class ParametrizacionDelete(LoginRequiredMixin, DeleteView):
    model = Parametrizacion
    success_url = reverse_lazy('parametrizacion:list')
    template_name = 'parametrizacion/delete.html'