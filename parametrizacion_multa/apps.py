from django.apps import AppConfig


class ParametrizacionMultaConfig(AppConfig):
    name = 'parametrizacion_multa'
