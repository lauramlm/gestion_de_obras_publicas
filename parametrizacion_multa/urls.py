from django.urls import path
from .views import ParametrizacionList, ParametrizacionCreate, ParametrizacionDelete, ParametrizacionUpdate
app_name = 'parametrizacion_multa'

urlpatterns = [
    path('list/',ParametrizacionList.as_view(), name='list'),
    path('create/',ParametrizacionCreate.as_view(), name='create'),
    path('update/<int:pk>/', ParametrizacionUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', ParametrizacionDelete.as_view(), name='delete'),

]