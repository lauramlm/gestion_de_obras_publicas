from django.db import models

from contratista.models import Contratista
from tipo_obra.models import Tipo_obra
from parametrizacion_multa.models import Parametrizacion
from .managers import WorkManager

class Obra(models.Model):
    tipo_obra = [
        ("Contrucción", "Construcción"),
        ("Remodelación", "Remodelación"),
        ("Reconstruccíon", "Reconstrucción"),
        ("Edificación", "Edificación")

    ]

    nombre= models.CharField(
        max_length=60, 
        unique=True,
        help_text="Ingresar solo letras")

    direccion = models.CharField(
        max_length=30,
        help_text="Ingresar una dirección valida")

    
     
    valor = models.IntegerField(
        help_text="Ingresar solo números")
    
    

    fecha_de_inicio = models.DateField(
        help_text="Ingresar la fecha dd MM yy")

    fecha_de_fin = models.DateField(
        help_text="Ingresar la fecha dd MM yy")

    estado = models.BooleanField(
        "Activo", 
        default=True)

    multa = models.IntegerField(default=0, blank=True, null=True)

    contratista = models.ForeignKey(
        Contratista, 
        on_delete=models.CASCADE,
        help_text="Ingrese nombre del contratista encargado")

    tipo_obra = models.CharField(
        max_length = 30,
        choices = tipo_obra
    )    

    parametrizacion_multa = models.ForeignKey(
         Parametrizacion,
         on_delete=models.CASCADE,
         help_text="Multa"
    )

    estados = WorkManager()

    def valor_multa(self, multa):
        if multa > 0:
            self.multa = multa
            self.save(update_fields=['multa'])   

    def mostrar_estado(self):
        if self.estado:
            mostrar_estado = "Activo"
        else:
            mostrar_estado = "Inactivo"
        
        return mostrar_estado
    
    def finalizar(self):
        if self.estado:
            self.estado = False
            self.save(update_fields=['estado'])

    def valor_multa(self, multa):
        if multa > 0:
            self.multa = multa
            self.save(update_fields=['multa'])

    def __str__(self):
        cadena = "{0}, {1}"
        return cadena.format(
            self.nombre, 
            self.direccion)
