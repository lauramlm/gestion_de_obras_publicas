import re
from django.forms import ModelForm, HiddenInput

from .models import Obra

class ObraForm(ModelForm):
    
    class Meta:
        model = Obra
        fields = '__all__'  

        widgets = {'estado':HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(ObraForm, self).__init__(*args, **kwargs)

        for visible in self.visible_fields():
            attrs = {'class': 'form-control', 'placeholder': visible.label}
            

            visible.field.widget.attrs.update(attrs)

            if self.errors.get(visible.name, None):
                visible.field.widget.attrs.update({'class': 'form-control is-invalid'})

class ObraFinishForm(ModelForm):
    class Meta:
        model = Obra
        fields = ('estado',)
