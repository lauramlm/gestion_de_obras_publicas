from datetime import date
from django.db import models

hoy = date.today()


class WorkQuerySet(models.QuerySet):
    def valids(self):
        return self.filter(fecha_de_fin__gte=hoy, estado=True)

    def finisheds(self):
        return self.filter(estado=False)

    def expireds(self):
        return self.filter(fecha_de_fin__lt=hoy, estado=True)


class WorkManager(models.Manager):
    def get_queryset(self):
        return WorkQuerySet(self.model, using=self._db)

    def valids(self):
        return self.get_queryset().valids()

    def finisheds(self):
        return self.get_queryset().finisheds()

    def expireds(self):
        return self.get_queryset().expireds()
