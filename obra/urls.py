from django.urls import path
from .views import  ObraList, ObraCreate, ObraExpiredList, ObraUpdate, ObraDelete, ObraFinish, ObraFinishedList
from .reportes.obras import ReporteObra

app_name = 'obra'

urlpatterns = [
    path('list/', ObraList.as_view(), name='list'),
    path('expired/', ObraExpiredList.as_view(), name='list-exprired'),
    path('finished/', ObraFinishedList.as_view(), name='list-finished'),
    path('create/', ObraCreate.as_view(), name='create'),
    path('update/<int:pk>/', ObraUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', ObraDelete.as_view(), name='delete'),
    path('report/<int:pk>/', ReporteObra.as_view(), name='report'),
    path('finish/<int:pk>/', ObraFinish.as_view(), name='finish'),
]
