from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Obra
from .forms import ObraForm, ObraFinishForm
from .utils import calcular_multa

class ObraList (LoginRequiredMixin, ListView):
    model = Obra
    template_name = "obra/index.html"
    queryset = Obra.estados.valids()

class ObraExpiredList(LoginRequiredMixin, ListView):
    model = Obra
    template_name = "obra/index.html"
    queryset = Obra.estados.expireds()

    def get_queryset(self):
        queryset = super(ObraExpiredList, self).get_queryset()

        for query in queryset:
        
            multa = calcular_multa(query)
            query.valor_multa(multa)

        return queryset

class ObraFinishedList(LoginRequiredMixin, ListView):
    model = Obra
    template_name = "obra/index.html"
    queryset = Obra.estados.finisheds()

class ObraCreate(LoginRequiredMixin, CreateView):
    model = Obra
    form_class = ObraForm
    success_url = reverse_lazy('obra:list')
    template_name = 'obra/create.html'

class ObraUpdate(LoginRequiredMixin, UpdateView):
    model = Obra
    form_class = ObraForm
    success_url = reverse_lazy('obra:list')
    template_name = 'obra/update.html'

class ObraDelete(LoginRequiredMixin, DeleteView):
    model = Obra
    success_url = reverse_lazy('obra:list')
    template_name = 'obra/delete.html' 

class ObraFinish(LoginRequiredMixin, UpdateView):
    model = Obra
    form_class = ObraFinishForm
    success_url = reverse_lazy('obra:list-finished')
    template_name = 'obras/update.html'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.finalizar()
        form = self.get_form()
        return self.form_valid(form)


    
