from datetime import date


__all__ = ('days_between', 'calcular_multa')

hoy = date.today()


def days_between(d1, d2):
    return abs(d2 - d1).days


def calcular_multa(contrato):
    #import ipdb
    #ipdb.set_trace()
    dias_contrato = days_between(contrato.fecha_de_inicio, contrato.fecha_de_fin)
    dias_atrazados = days_between(contrato.fecha_de_fin, hoy)
    parametrizacion_multas = contrato.parametrizacion_multa
    porcentaje_desfase_incremental = parametrizacion_multas.porcentaje_desfase_incremental
    porcentaje_desfase = parametrizacion_multas.porcentaje_desfase
    porcentaje_valor_multa = parametrizacion_multas.porcentaje_valor_multa

    try:
        porcentaje_atrazado = (dias_atrazados * 100) / dias_contrato
    except ZeroDivisionError:
        porcentaje_atrazado = 0

    valor_multa = (contrato.valor * porcentaje_valor_multa) / 100
    valor_desfase = 0
    while porcentaje_atrazado > porcentaje_desfase_incremental:
        valor_desfase += (contrato.valor * porcentaje_desfase) / 100
        porcentaje_desfase_incremental += 10

    valor_total = valor_multa + valor_desfase

    return round(valor_total, 2)
